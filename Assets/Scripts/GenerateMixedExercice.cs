using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GenerateMixedExercice : MonoBehaviour 
{
    public GameObject Movement;
    public GameObject Posture;
    List<GameObject> Movements;

    void Awake()
    {
        Movement = GameManager.Instance.Movement;
        Posture = GameManager.Instance.Posture;
    }

	void Start () 
    {
        Movements = new List<GameObject>();

        GameObject obj = Instantiate(Posture) as GameObject;
        obj.transform.parent = transform;
        obj.AddComponent<StandStraightPost>();

        //Generate Movements
        for (int i = 0; i < 3; i++)
        {
            GameObject objMvt = Instantiate(Movement) as GameObject;

            objMvt.transform.parent = transform;
            objMvt.SetActive(false);
            Movements.Add(objMvt);
        }

        Movements[0].AddComponent<ExternalRotationDetect>();
        Movements[0].SetActive(true);
        Movements[1].AddComponent<AbductionGest>();
        Movements[2].AddComponent<ElevationGest>();
	}
}
