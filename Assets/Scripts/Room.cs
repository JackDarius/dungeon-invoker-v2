using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Element { Fire, Water, Plant};

public class Room : MonoBehaviour 
{
    private Element _element;

    void Start()
    {
    }

    void Update()
    {
        //On test ici le gobelin courant, c est a dire celui qui est le plus proche du player
        float distance = 0f;
        foreach(Transform child in transform)
        {
            if (child.tag == "Gobelin")
            {
                if (distance == 0f || Vector3.Distance(GameManager.Instance.Player.transform.position, child.position) < distance)
                {
                    GameManager.Instance.CurrentGobelin = child.gameObject;
                    //Debug.Log("TEST");
                }
            }
        } 
    }

    /*********Getter-Setter*********/
    public Element Element
    {
        get { return _element; }
        set { _element = value; }
    }
}
