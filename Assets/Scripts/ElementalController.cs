﻿using UnityEngine;
using System.Collections;

public class ElementalController : MonoBehaviour
{
    public float Speed;
    private Element _type;
    private Material _material;

    void Start ()
    {
        _material = new Material(Shader.Find("Sprites/Default"));
        if (_type == Element.Plant)
            _material.color = Color.green;
        if (_type == Element.Fire)
            _material.color = Color.red;
        if (_type == Element.Water)
            _material.color = Color.blue;
        GetComponent<SpriteRenderer>().material = _material;
    }
	
	void Update ()
    {
	
	}

    public Element Type
    {
        get { return _type; }
        set { _type = value; }
    }
}
