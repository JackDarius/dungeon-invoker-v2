﻿using UnityEngine;
using Windows.Kinect;

public class StandStraightPost : MonoBehaviour
{

    private Vector3 _spineHeadAxe;
    private Vector3 _spineAxe;
    private Vector3 _shouldersAxe;
    private Vector3 _hipAxe;

    private float _angleShoulderSpine = 0f;
    private float _angleHipSpine = 0f;

    private float _angleShoulderSpineRef = 0f;
    private float _angleSpineCurrentRef = 0f;
    private float _angleHipSpineRef = 0f;

    public bool doSomething = false;

    void Detect(Body user)
    {

        _spineHeadAxe = user.Joints[JointType.SpineBase].GetJointPosition() -
                        user.Joints[JointType.Head].GetJointPosition();

        _spineAxe = user.Joints[JointType.SpineBase].GetJointPosition() -
                     user.Joints[JointType.SpineShoulder].GetJointPosition();

        _shouldersAxe = user.Joints[JointType.ShoulderLeft].GetJointPosition() -
                        user.Joints[JointType.ShoulderRight].GetJointPosition();

        _hipAxe = user.Joints[JointType.HipLeft].GetJointPosition() -
                     user.Joints[JointType.HipRight].GetJointPosition();

        _angleShoulderSpine = Vector3.Angle(_spineAxe, _shouldersAxe);
        _angleHipSpine = Vector3.Angle(_spineAxe, _hipAxe);

        _angleShoulderSpineRef = Vector3.Angle(GameManager.Instance.ReferencePlan[0], GameManager.Instance.ReferencePlan[1]);
        _angleSpineCurrentRef = Vector3.Angle(_spineHeadAxe, GameManager.Instance.ReferencePlan[1]);
        _angleHipSpineRef = Vector3.Angle(GameManager.Instance.ReferencePlan[2], GameManager.Instance.ReferencePlan[1]);

        //Posture Dos droit: Test si epaules decalees par rapport a sa colonne vertebrale et colonne vertebrale a angle droit par rapport a l'axe de ses hanches
        if ((_angleShoulderSpine > _angleShoulderSpineRef - 10f && _angleShoulderSpine < _angleShoulderSpineRef + 10f) &&
            (_angleHipSpine > _angleHipSpineRef - 10f && _angleHipSpine < _angleHipSpineRef + 10f))
        {
            //Posture Dos Droit: Test si toutes les articulations de la colonne vertebrale sont alignes par rapport a l'axe de la colonne vertebrale de reference
            if ((_angleSpineCurrentRef > 0f && _angleSpineCurrentRef < 10f))
            {
                doSomething = true;
            }
            else
                doSomething = false;
        }
        else
            doSomething = false;
    }
}   
