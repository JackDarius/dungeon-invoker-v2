using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GenerateLevel : MonoBehaviour 
{
    public GameObject Room;
    public GameObject SpawnGobelin;
    public GameObject SpawnLimit;
    public GameObject Player;
    public GameObject Elemental;
    public GameObject Exercice;
    public List<Sprite> PlayerSkins;
    public float range = 4;

    public int NbrRoom;
    public int NbrGobelin;
    List<GameObject> Gobelins;
    List<GameObject> Rooms;

	void Start () 
    {
        Rooms = new List<GameObject>();
        Gobelins = new List<GameObject>();

        //Generate Rooms
        for (int i = 0; i < NbrRoom; i++)
        {
            System.Random rndElement = new System.Random();
            GameObject obj = Instantiate(Room) as GameObject;
            if (rndElement.Next(3) < 1)
                obj.GetComponent<Room>().Element = Element.Fire;
            if (rndElement.Next(3) >= 2)
                obj.GetComponent<Room>().Element = Element.Water;
            if (rndElement.Next(3) >= 1 && rndElement.Next(3) < 2)
                obj.GetComponent<Room>().Element = Element.Plant;
            obj.SetActive(false);
            Rooms.Add(obj);
        }

        //Init the first current room
        GameManager.Instance.CurrentRoom = Rooms[0];
        GameManager.Instance.CurrentRoom.SetActive(true);

        GameManager.Instance.Rooms = Rooms;

        //Generate Gobelins
        for (int i = 0; i < NbrGobelin; i++)
        {
            GameObject obj = Instantiate(SpawnGobelin) as GameObject;
            Gobelins.Add(obj);
        }
        AddGobelinsRandomlyInRooms();

        //Generate Player
        GameObject player = Instantiate(Player) as GameObject;
        player.transform.position = GameManager.Instance.PlayerSpawn.transform.position;
        GameManager.Instance.Player = player;
        GameManager.Instance.MovingToFightAnimator = player.GetComponent<Animator>();
        GameManager.Instance.MovingToFightAnimator.enabled = false;
        player.transform.parent = Rooms[0].transform;
        GameManager.Instance.PlayerController = player.GetComponent<PlayerController>();

        //Generate Elementary
        GameObject elemental = Instantiate(Elemental) as GameObject;
        elemental.transform.parent = player.transform;
        elemental.transform.position = new Vector2(player.transform.position.x + 3f, player.transform.position.y - 1f);
        GameManager.Instance.Elemental = elemental;
        elemental.SetActive(false);

        //Generate Exercice
        GameObject exercice = Instantiate(Exercice) as GameObject;
        GameManager.Instance.Exercice = exercice;

        if (GameManager.Instance.Topic == Topic.Mixed)
        {
            player.GetComponent<SpriteRenderer>().sprite = PlayerSkins[0];
            exercice.AddComponent<GenerateMixedExercice>();
        }
	}

    /**
     * Add Randomly Gobelins In rooms  (with randomly position)
     **/
    void AddGobelinsRandomlyInRooms()
    {
        int compteur = 0;
        int nbrGobInRoom = 0;
        int nbrGobelins = Gobelins.Count;
        List<GameObject> gob = Gobelins;

        while (compteur < nbrGobelins)
        {
            foreach (GameObject room in Rooms)
            {
                if (compteur >= nbrGobelins) { return; }

                System.Random rnd = new System.Random();

                if (room == Rooms[0]) { nbrGobInRoom = 0; }
                if (room == Rooms[1] && compteur == 0) { nbrGobInRoom = 1; }

                if (room != Rooms[0] && room != Rooms[1])
                {
                    if (nbrGobelins - compteur >= 2)
                        nbrGobInRoom = rnd.Next(0, 3); //Random Integer from 0 to 2!!!
                    else
                        nbrGobInRoom = rnd.Next(0, (nbrGobelins - compteur) + 1);
                }
                
                for (int i = 0; i <= nbrGobInRoom - 1; i++)
                {
                    gob[gob.Count - 1].transform.parent = room.transform;
                    gob.RemoveAt(gob.Count - 1);
                    compteur++;
                    // Random Position
                    /*gob[gob.Count - 1].transform.position = new Vector2(Random.Range(gob[gob.Count - 1].transform.position.x - 2f,
                                                                        SpawnLimit.transform.position.x), gob[gob.Count - 1].transform.position.y);*/
                }
            }
        }
    }
}
