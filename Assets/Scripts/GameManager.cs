using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Windows.Kinect;

public enum Topic { Mixed, JointMobility, Strengthening };

public class GameManager : MonoBehaviour
{
    //Kinect
    public GameObject Movement;
    public GameObject Posture;
    public GameObject BodySourceManagerGo;
    public GameObject CurrentMovement;
    public GameObject CurrentPosture;
    public BodySourceManager BodySourceManager;
    public Body user;
    public List<Vector3> ReferencePlan;


    //Game elements
    public GameObject Player;
    public GameObject Elemental;
    public GameObject Exercice;
    public GameObject CurrentGobelin;
    public GameObject Goal;
    public GameObject Background;
    public GameObject WallDoors;
    public GameObject PlayerSpawn;
    public GameObject GobelinSpawn;
    public PlayerController PlayerController;
    public enum Phase { Exploration, Attaque, Invocation };
    public List<GameObject> SpritesParents;
    public Animator MovingToFightAnimator;

    //UI
    public Image ChangeRoomAnimatiomImg;
    public Image RestartRoomImg;
    public Image EnergyBar;
    public Text PhaseTxt;
    public Text RanimationTxt;
    public Animator ChangeRoomAnimator;
    public Animator RestartRoomAnimator;
    public Animator RanimationAnimator;
    public Animator PhaseAnimator;
    public Canvas CursorForeground;
    public GameObject EnergyParticleBar;
    public GameObject BadCursor;
    public GameObject MediumCursor;
    public GameObject GoodCursor;
    public Sprite BlueCursor;
    public Sprite RedCursor;
    public Sprite GreenCursor;
    public float speedGaugeFilling = 0.01f;
    public float waitTime = 4f;

    private Phase _phase = Phase.Exploration;
    private Topic _topic;
    private List<GameObject> _rooms;
    private GameObject _currentRoom;
    private bool _synchroCheck = false;
    private bool _fightBegining = false;

    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            if(_instance == null)
            {
                Debug.LogError("Controller is not instanciated yet");
                return null;
            }
            return _instance;
        }
    }

    void Awake()
    {
        _instance = this;
        BodySourceManager = BodySourceManagerGo.GetComponent<BodySourceManager>();
    }

    void Start()
    {
        ReferencePlan = new List<Vector3>();
        EnergyBar.fillAmount = 0;
        CursorForeground.gameObject.SetActive(false);
        EnergyBar.gameObject.SetActive(false);
        ChangeRoomAnimator.enabled = false;
        RestartRoomAnimator.enabled = false;
        PhaseAnimator.enabled = false;
        RanimationAnimator.enabled = false;
        RanimationTxt.gameObject.SetActive(false);
        RestartRoomImg.gameObject.SetActive(false);
        EnergyParticleBar.SetActive(false);
        StartCoroutine(RunAnimPhaseTxt());
    }

    void Update()
    {
        RescaleSprite();

        if (!_synchroCheck)
            Synchronisation();

        DetectCurrentMovementPosture();

        if (CurrentMovement != null && CurrentPosture != null && _synchroCheck)
        {
            //Test si le dos du patient est bien droit
            if (CurrentPosture.GetComponent<StandStraightPost>() != null)
                DetectStandStraightPost();
        }
    }

    void Synchronisation()
    {
        //Recupere le premier utilisateur synchronise
        //Body[] data = GameManager.Instance.BodySourceManager.GetData();
        user = GameManager.Instance.BodySourceManager.getFirstTrackedUser();

        if (user == null) { return; }
        
        float angleForearmArmLeft = Vector3.Angle(user.Joints[JointType.ShoulderLeft].GetJointPosition() - user.Joints[JointType.ElbowLeft].GetJointPosition(),
                                                  user.Joints[JointType.ElbowLeft].GetJointPosition() - user.Joints[JointType.HandLeft].GetJointPosition());
        float angleArmLeftShoulder = Vector3.Angle(user.Joints[JointType.ShoulderLeft].GetJointPosition() - user.Joints[JointType.ElbowLeft].GetJointPosition(),
                                                   user.Joints[JointType.ShoulderRight].GetJointPosition() - user.Joints[JointType.ShoulderLeft].GetJointPosition());

        if ((angleForearmArmLeft > 85 && angleForearmArmLeft < 95) && (angleArmLeftShoulder > 0 && angleArmLeftShoulder < 5))
        {
            Debug.Log("TEST si on peut me synchro");
        }
        Debug.Log("Synchro OK");

        //Calcul des axes referentiel 
        Vector3 shoulderAxeRef = user.Joints[JointType.ShoulderLeft].GetJointPosition() -
                                 user.Joints[JointType.ShoulderRight].GetJointPosition();
        Vector3 spineAxeRef = user.Joints[JointType.SpineBase].GetJointPosition() -
                              user.Joints[JointType.SpineShoulder].GetJointPosition();
        Vector3 hipAxeRef = user.Joints[JointType.HipLeft].GetJointPosition() -
                            user.Joints[JointType.HipRight].GetJointPosition();
        Vector3 armRightRef = user.Joints[JointType.ShoulderRight].GetJointPosition() -
                              user.Joints[JointType.ElbowRight].GetJointPosition();
        Vector3 forward = Vector3.Cross(shoulderAxeRef, spineAxeRef); //Left Hand Rule

        if (ReferencePlan.Count == 0)
        {
            ReferencePlan.Add(shoulderAxeRef);
            ReferencePlan.Add(spineAxeRef);
            ReferencePlan.Add(hipAxeRef);
            ReferencePlan.Add(armRightRef);
            ReferencePlan.Add(forward);
        }
        _synchroCheck = true;
    }

    void DetectCurrentMovementPosture()
    {
        foreach (Transform child in GameManager.Instance.Exercice.transform)
        {
            if (child.tag == "Movement")
            {
                if (child.gameObject.activeInHierarchy)
                    CurrentMovement = child.gameObject;
            }
            if (child.tag == "Posture")
            {
                if (child.gameObject.activeInHierarchy)
                    CurrentPosture = child.gameObject;
            }
        }
    }

    /*Send a message to detect the current movement*/
    void DetectStandStraightPost()
    {
        CurrentPosture.SendMessage("Detect", user);

        //Si le dos du patient a une bonne posture, dans ce cas on test le mouvement courant a faire
        if (CurrentPosture.GetComponent<StandStraightPost>().doSomething)
        {
            if (CurrentMovement.GetComponent<ExternalRotationDetect>() != null)
                DetectExternalRotation();
            if (CurrentMovement.GetComponent<AbductionGest>() != null)
                DetectAbduction();
            if (CurrentMovement.GetComponent<ElevationGest>() != null)
                DetectElevation();
        }

    }

    void DetectExternalRotation()
    {
        if (!CurrentMovement.GetComponent<ExternalRotationDetect>().doSomething)
            CurrentMovement.SendMessage("Detect", user);

        if (CurrentMovement.GetComponent<ExternalRotationDetect>().doSomething)
            StartCoroutine(PlayerController.Move(Goal.transform, 7f));
    }
    
    void DetectAbduction()
    {
        CurrentMovement.SendMessage("Detect", user);

        if (CurrentMovement.GetComponent<AbductionGest>().doSomething && CurrentMovement.GetComponent<AbductionGest>()._isReturn)
            EnergyBar.fillAmount += 1.0f / waitTime * Time.deltaTime;
        else if(!CurrentMovement.GetComponent<AbductionGest>().doSomething && CurrentMovement.GetComponent<AbductionGest>()._isReturn)
        {
            if (EnergyBar.fillAmount < 0.36f)
                Elemental.GetComponent<ElementalController>().Type = DetectElementAccordingCursor(BadCursor);

            if (EnergyBar.fillAmount >= 0.36f && EnergyBar.fillAmount < 0.73f)
                Elemental.GetComponent<ElementalController>().Type = DetectElementAccordingCursor(MediumCursor);

            if (EnergyBar.fillAmount >= 0.73f)
                Elemental.GetComponent<ElementalController>().Type = DetectElementAccordingCursor(GoodCursor);

            Elemental.SetActive(true);
            _phase = Phase.Invocation;
            CurrentMovement.GetComponent<AbductionGest>()._isReturn = false;
            ChangeCurrentMovement();
        }
    }

    void DetectElevation()
    {

    }

    void ChangeCurrentMovement()
    {
        if (_phase == Phase.Attaque)
        {
            foreach (Transform child in GameManager.Instance.Exercice.transform)
            {
                if (child.tag == "Movement")
                {
                    if (child.gameObject.activeInHierarchy)
                        child.gameObject.SetActive(false);
                    if (child.gameObject.GetComponent<AbductionGest>())
                        child.gameObject.SetActive(true);
                }
            }
        }
        if (_phase == Phase.Invocation)
        {
            foreach (Transform child in GameManager.Instance.Exercice.transform)
            {
                if (child.tag == "Movement")
                {
                    if (child.gameObject.activeInHierarchy)
                        child.gameObject.SetActive(false);
                    if (child.gameObject.GetComponent<ElevationGest>())
                        child.gameObject.SetActive(true);
                }
            }
        }
    }

    //Lancement d'un changement de salle avec animation (alpha modifi� d'une texture)
    public IEnumerator ChangeRoom()
    {
        while (GameManager.Instance.ChangeRoomAnimatiomImg.color.a < 1f)
            yield return null;

        EnergyBar.fillAmount = 0;
        EnergyParticleBar.SetActive(false);
        EnergyBar.gameObject.SetActive(false);
        CursorForeground.gameObject.SetActive(false);
        CurrentRoom.SetActive(false);
        CurrentRoom = GameManager.Instance.Rooms[GameManager.Instance.Rooms.IndexOf(GameManager.Instance.CurrentRoom) + 1];
        Player.transform.parent = GameManager.Instance.CurrentRoom.transform;
        CurrentRoom.SetActive(true);
        Player.transform.position = GameManager.Instance.PlayerSpawn.transform.position;

        //Si je suis dans une salle qui a des gobelins je change ma phase
        if (GameManager.Instance.CurrentRoom.gameObject.transform.childCount > 1)
        {
            _phase = Phase.Attaque;
            ChangeCurrentMovement();

            float i = 0f;

            foreach (Transform child in GameManager.Instance.CurrentRoom.transform)
            {
                if (child.tag == "Gobelin")
                {
                    child.transform.position = new Vector2(GobelinSpawn.transform.position.x + i, child.transform.position.y);
                    i = i + 3;
                }
            }
        }

        StartCoroutine(DisabledFadeAnimation());
    }

    //Cette couroutine ne peut etre lance que dans le cas ou un gobelin aurait tue le player
    public IEnumerator RestartRoom()
    {
        yield return new WaitForSeconds(1f);

        _phase = Phase.Attaque;
        ChangeCurrentMovement();
        RanimationTxt.gameObject.SetActive(true);
        RanimationAnimator.enabled = true;
        Elemental.SetActive(false);
        EnergyBar.fillAmount = 0;
        EnergyParticleBar.SetActive(false);
        EnergyBar.gameObject.SetActive(false);
        CursorForeground.gameObject.SetActive(false);

        Player.transform.position = GameManager.Instance.PlayerSpawn.transform.position;

        float i = 0f;

        foreach (Transform child in GameManager.Instance.CurrentRoom.transform)
        {
            if (child.tag == "Gobelin")
            {
                child.transform.position = new Vector2(GobelinSpawn.transform.position.x + i, child.transform.position.y);
                i = i + 3;
            }    
        }

        yield return new WaitForSeconds(5f);

        RanimationAnimator.Play("RanimationLoad", -1, 0f);
        RestartRoomAnimator.Play("RestartRoom", -1, 0f);
        RanimationAnimator.enabled = false;
        RestartRoomAnimator.enabled = false;
        RanimationTxt.gameObject.SetActive(false);
        RestartRoomImg.gameObject.SetActive(false);

        StartCoroutine(RunAnimPhaseTxt());
    }

    //Fonction qui continue l'animation de fondue (changement de l'alpha d'une texture)
    IEnumerator DisabledFadeAnimation()
    {
        while (GameManager.Instance.ChangeRoomAnimatiomImg.color.a > 0f)
            yield return null;
        ChangeRoomAnimator.Play("ChangingRoom", -1, 0f);
        ChangeRoomAnimator.enabled = false;
        StartCoroutine(RunAnimPhaseTxt());
    }

    /*
     * Lance une animation a chaque debut de changement de salle 
     * si pas de gobelin: Phase d'exploration 
     * si gobelin present alors: Phase Attaque
     */
    IEnumerator RunAnimPhaseTxt()
    {
        PhaseAnimator.enabled = true;
        if (_phase == Phase.Exploration)
            PhaseTxt.text = "Phase Exploration";
        if (_phase == Phase.Attaque)
            PhaseTxt.text = "Phase Attaque";
        
        yield return new WaitForSeconds(5.0f);

        PhaseAnimator.Play("DisplayPhase", -1, 0f);
        PhaseAnimator.enabled = false;

        if (_phase == Phase.Attaque)
        {
            CursorForeground.gameObject.SetActive(true);
            ChangePositionCursorAccordingCurrentGobelin();
            EnergyBar.gameObject.SetActive(true);
            MovingToFightAnimator.enabled = true;
            StartCoroutine(RunMovingToFight());
        }
    }

    IEnumerator RunMovingToFight()
    {
        yield return new WaitForSeconds(2.0f);
        MovingToFightAnimator.Play("MovingToFight", -1, 0f);
        _fightBegining = true;
        MovingToFightAnimator.enabled = false;
    }


    Element DetectElementAccordingCursor(GameObject cursor)
    {
        Element el = Element.Fire;

        if (cursor.GetComponent<Image>().sprite.name.Contains("green"))
            el = Element.Plant;
        if (cursor.GetComponent<Image>().sprite.name.Contains("blue"))
            el = Element.Water;
        if (cursor.GetComponent<Image>().sprite.name.Contains("red"))
            el = Element.Fire;

        return el;
    }

    void ChangePositionCursorAccordingCurrentGobelin()
    {
        foreach (Transform child in CursorForeground.transform)
        {
            if (CurrentGobelin.GetComponent<GobelinController>().Type == Element.Fire)
            {
                if (child.name == "BadCursor")
                    BadCursor.GetComponent<Image>().sprite = GreenCursor; 
                if (child.name == "MediumCursor")
                    MediumCursor.GetComponent<Image>().sprite = RedCursor;
                if (child.name == "GoodCursor")
                    GoodCursor.GetComponent<Image>().sprite = BlueCursor;
            }

            if (CurrentGobelin.GetComponent<GobelinController>().Type == Element.Plant)
            {
                if (child.name == "BadCursor")
                    BadCursor.GetComponent<Image>().sprite = BlueCursor;
                if (child.name == "MediumCursor")
                    MediumCursor.GetComponent<Image>().sprite = GreenCursor;
                if (child.name == "GoodCursor")
                    GoodCursor.GetComponent<Image>().sprite = RedCursor;
            }

            if (CurrentGobelin.GetComponent<GobelinController>().Type == Element.Water)
            {
                if (child.name == "BadCursor")
                    BadCursor.GetComponent<Image>().sprite = RedCursor;
                if (child.name == "MediumCursor")
                    MediumCursor.GetComponent<Image>().sprite = BlueCursor;
                if (child.name == "GoodCursor")
                    GoodCursor.GetComponent<Image>().sprite = GreenCursor;
            }
        }
    }

    void RescaleSprite()
    {
        if (SpritesParents.Count < 3)
            SpritesParents.Add(Player);

        for (int i = 0; i < SpritesParents.Count; i++)
            SpriteFunctions.ResizeSpriteToScreen(SpritesParents[i], Camera.main, 1, 1);
    }

    /*********Getter-Setter*********/
    public Topic Topic
    {
        get { return _topic; }
        set { _topic = value; }
    }

    public List<GameObject> Rooms
    {
        get { return _rooms; }
        set { _rooms = value; }
    }

    public GameObject CurrentRoom
    {
        get { return _currentRoom; }
        set { _currentRoom = value; }
    }

    public bool IsFight
    {
        get { return _fightBegining; }
        set { _fightBegining = value; }
    }
}
