using UnityEngine;
using System.Collections;

public class GobelinController : MonoBehaviour
{
    public float Speed;
    private Element _type;
    private Material _material;

    void Start () 
    {
        _type = transform.parent.GetComponent<Room>().Element;

        _material = new Material(Shader.Find("Sprites/Default"));
        if(_type == Element.Plant)
            _material.color = Color.green;
        if (_type == Element.Fire)
            _material.color = Color.red;
        if (_type == Element.Water)
            _material.color = Color.blue;
        GetComponent<SpriteRenderer>().material = _material;
    }
	
	void Update ()
    {
        if (GameManager.Instance.IsFight)
            transform.position = Vector3.MoveTowards(transform.position, new Vector2(GameManager.Instance.Player.transform.position.x, transform.position.y), Speed * Time.deltaTime);
    }

    /*********Getter-Setter*********/
    public Element Type
    {
        get { return _type; }
        set { _type = value; }
    }
}
