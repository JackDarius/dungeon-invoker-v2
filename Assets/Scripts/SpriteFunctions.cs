using UnityEngine;
using System.Collections;

public static class SpriteFunctions 
{
    public static void ResizeSpriteToScreen(GameObject element, Camera theCamera, int fitToScreenWidth, int fitToScreenHeight)
    {
        float height = 0f;
        float width = 0f;

        float worldScreenHeight = theCamera.orthographicSize * 2.0f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        element.transform.localScale = new Vector3(1, 1, 1);

        if (element.tag == "Wall")
        {
            foreach (Transform child in element.transform)
            {
                if (child.tag == "Ground")
                    break;

                if (height == 0f)
                    height = child.GetComponent<SpriteRenderer>().sprite.bounds.size.y;

                width += child.GetComponent<SpriteRenderer>().sprite.bounds.size.x;
            }

            if (fitToScreenWidth != 0)
                element.transform.localScale = new Vector3(((worldScreenWidth + 2f)/ width) * fitToScreenWidth,
                                                            ((worldScreenWidth + 2f)/ width) * fitToScreenWidth,
                                                            element.transform.localScale.z);

            element.transform.position = new Vector3(element.transform.position.x,
                                                  -theCamera.orthographicSize/1.8f,
                                                  element.transform.position.z);
        }

        if (element.tag == "Player")
        {
            if (fitToScreenWidth != 0)
                element.transform.localScale = GameManager.Instance.WallDoors.transform.localScale / 3f;
        }

        if (element.tag == "Background")
        {
            foreach (Transform child in element.transform)
            {
                height = child.GetComponent<SpriteRenderer>().sprite.bounds.size.y;
                width = child.GetComponent<SpriteRenderer>().sprite.bounds.size.x;

                if (fitToScreenWidth != 0)
                    element.transform.localScale = new Vector3((worldScreenWidth / width) * fitToScreenWidth,
                                                                (worldScreenWidth / width) * fitToScreenWidth,
                                                                element.transform.localScale.z);
            }
        }
    }

}
