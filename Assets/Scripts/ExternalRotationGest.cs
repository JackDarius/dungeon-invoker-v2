using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Windows.Kinect;

public class ExternalRotationDetect : MonoBehaviour 
{
    private Vector3 _armRight;
    private Vector3 _forearmRight;
    private Vector3 _shouldersAxe;

    private float _angleArmShoulder = 0f;
    private float _angleForearmArm = 0f;
    private float _angleForearmShoulder = 0f;

    private float _angleArmShoulderRef = 0f;

    private bool _isReturn = false;
    public bool doSomething = false;

    void Detect(Body user)
    {
        _armRight = user.Joints[JointType.ShoulderRight].GetJointPosition() -
                    user.Joints[JointType.ElbowRight].GetJointPosition();

        _forearmRight = user.Joints[JointType.HandRight].GetJointPosition() -
                        user.Joints[JointType.ElbowRight].GetJointPosition();

        _shouldersAxe = user.Joints[JointType.ShoulderLeft].GetJointPosition() -
                        user.Joints[JointType.ShoulderRight].GetJointPosition();

        _angleArmShoulder = Vector3.Angle(_armRight, _shouldersAxe);
        _angleForearmShoulder = Vector3.Angle(_forearmRight, _shouldersAxe);
        _angleForearmArm = Vector3.Angle(_forearmRight, _armRight);

        _angleArmShoulderRef = Vector3.Angle(GameManager.Instance.ReferencePlan[3], GameManager.Instance.ReferencePlan[0]);
        
        //Test si le bras est bien colle au corps en fonction de la morphologie de base de l'utilisateur (le patient)
        if (_angleArmShoulder > _angleArmShoulderRef - 10f && _angleArmShoulder < _angleArmShoulderRef + 10f)
        {
            if (_angleForearmArm > 75f && _angleForearmArm < 105f)
            {
                if (_angleForearmShoulder > 0f && _angleForearmShoulder < 100f)
                {
                    if ((_angleForearmShoulder > 0f && _angleForearmShoulder < 30f) && !_isReturn)
                    {
                        _isReturn = true;
                    }
                    else if ((_angleForearmShoulder < 100f && _angleForearmShoulder > 80f) && _isReturn) //Test si je suis bien a angle droit de mon ventre et que j'ai bien fait mon aller
                    {
                        _isReturn = false;
                        doSomething = true;
                    }
                }
            }
        }
    }
}
