using UnityEngine;
using System.Collections;
using Windows.Kinect;

public class AbductionGest : MonoBehaviour 
{
    private Vector3 _armRight;
    private Vector3 _foreArmRight;
    private Vector3 _spineAxe;
    private Vector3 _shouldersAxe;

    private float _angleArmShoulder = 0f;
    private float _angleArmShoulderRef = 0f;
    private float _angleArmSpinal = 0f;
    private float _angleForearmArm = 0f;

    public bool _isReturn = false;
    public bool doSomething = false;

    void Detect(Body user)
    {
        _armRight = user.Joints[JointType.ShoulderRight].GetJointPosition() -
                        user.Joints[JointType.ElbowRight].GetJointPosition();

        _foreArmRight = user.Joints[JointType.HandRight].GetJointPosition() -
                        user.Joints[JointType.ElbowRight].GetJointPosition();

        _spineAxe = user.Joints[JointType.SpineBase].GetJointPosition() -
                    user.Joints[JointType.SpineShoulder].GetJointPosition();

        _shouldersAxe = user.Joints[JointType.ShoulderLeft].GetJointPosition() -
                    user.Joints[JointType.ShoulderRight].GetJointPosition();

        _angleArmShoulder = Vector3.Angle(_armRight, _shouldersAxe);
        _angleForearmArm = Vector3.Angle(_armRight, _foreArmRight);
        _angleArmSpinal = Vector3.Angle(_armRight, _spineAxe);

        _angleArmShoulderRef = Vector3.Angle(GameManager.Instance.ReferencePlan[3], GameManager.Instance.ReferencePlan[0]);

        if (_angleArmShoulder > _angleArmShoulderRef - 20f && _angleArmShoulder < _angleArmShoulderRef + 20f)
        {
            if (_angleForearmArm > 150f && _angleForearmArm < 190f)
            {

                if (_angleArmSpinal > 80f && _angleArmSpinal < 180f)
                {
                    /*if ((_angleArmSpinal > 160f && _angleArmSpinal < 180f) && !_isReturn)
                    {
                        _isReturn = true;
                    }*/
                    if ((_angleArmSpinal > 80 && _angleArmSpinal < 100f) /*&& _isReturn*/) //Test si je suis bien a angle droit de mon ventre et que j'ai bien fait mon aller
                    {
                        _isReturn = true;
                        doSomething = true;
                    }
                    else
                        doSomething = false;
                }
                else
                    doSomething = false;
            }
            else
                doSomething = false;
        }
        else
            doSomething = false;
    }
}