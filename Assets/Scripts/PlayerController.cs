using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour 
{
    public float Speed;
    private string type;

    void Awake()
    {
    }

	void Start () 
    {
	}
	
	void Update ()
    {
	}

    public IEnumerator Move(Transform target, float speed)
    {
        transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);

        while (transform.position != target.position)
            yield return null;

        if(GameManager.Instance.CurrentMovement.GetComponent<ExternalRotationDetect>() != null)
            GameManager.Instance.CurrentMovement.GetComponent<ExternalRotationDetect>().doSomething = false;
    }
    
    public void InvokeElementary()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "EndRoom")
        {   
            GameManager.Instance.ChangeRoomAnimator.enabled = true;
            StartCoroutine(GameManager.Instance.ChangeRoom());
        }

        if (other.tag == "Gobelin")
        {
            GameManager.Instance.IsFight = false;
            GameManager.Instance.RestartRoomImg.gameObject.SetActive(true);
            GameManager.Instance.RestartRoomAnimator.enabled = true;
            StartCoroutine(GameManager.Instance.RestartRoom());
        }
    }
}
